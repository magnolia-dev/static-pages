# About
This repository is a minimal node webserver for static files, made to be deployed with docker. Powered by [serve](https://www.npmjs.com/package/serve).

# Structure
* `pages/` will be created at runtime if it doesn't exist, and is where static content will be served from. Other repos can deploy into `/pages/` while this application is running.
* `serve.json` specifies [options](https://www.npmjs.com/package/serve-handler) for the `serve` binary.
* `Dockerfile` dictates the starting state of the application container.
* `docker-compose.yml` is used to specify container options during runtime. Instead of running a verbose `docker` command, we can just run `docker-compose up`.

# Commands
* Start the application: `docker-compose up`
* Start the application in the background: `docker-compose up -d`
* Tear down containers relating to this application: `docker-compose down`
* Rebuild containers: `docker-compose build`
* List running containers: `docker ps`

# Deployment
* This application can be deployed using [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines). 
* `bitbucket-pipelines.yml` contains scripts that can be triggered by the pipeline.
* As usual, these require dynamic or sensitive values that shouldn't be hard-coded. These can be configured in [Repository Variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html)
* Pipeline operations can be triggered manually from the repository's pipelines tab in Bitbucket.

