FROM node:12

# Install serve at version 11
RUN npm install -g serve@11

# Copy in our serve config
COPY serve.json /serve.json

# Serve static directory in /pages over port 80
CMD serve /pages --config /serve.json -p 80

